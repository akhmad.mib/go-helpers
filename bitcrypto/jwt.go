package bitcrypto

import (
	"github.com/dgrijalva/jwt-go"
	"fmt"
	"time"
)


const (
	AUDIENCE = "TixID Middleware"
	ISSUER = "TixID Security Authority"
	SUBJ_AUTH_CODE = "Mobile authorization token"
	PURPOSE_LOGIN = "login"
	PURPOSE_NOTLOGIN = "notlogin"
	PURPOSE_REGISTER = "register"
	PURPOSE_RESET_PASSWORD = "reset_password"
	PURPOSE_CHANGE_MSISDN = "change_msisdn"
)

type TXJWTClaim struct {
	Msisdn string `json:"msisdn"`
	Purpose string `json:"purpose"`
	jwt.StandardClaims
}

func claimsToToken(claims jwt.Claims, key *[]byte) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(*key)
	if err != nil {
		log.Error(err.Error())
		return "", err
	}
	return tokenString, nil
}

//==============================================================================

func genAuthToken(key *[]byte, msisdn string, purpose string) (string, bool) {
	t := time.Now().UTC()

	var claims *TXJWTClaim

	switch purpose {
	case PURPOSE_NOTLOGIN:
		claims = &TXJWTClaim{
			Purpose:PURPOSE_NOTLOGIN,
			StandardClaims: jwt.StandardClaims{
				Subject:   SUBJ_AUTH_CODE,
				Issuer:    ISSUER,
				Audience:  AUDIENCE,
				IssuedAt:  t.Unix(),
				ExpiresAt: t.Add(timeOut).Unix(),
			},
	}
	case PURPOSE_LOGIN:
		claims = &TXJWTClaim{
			Msisdn:msisdn,
			Purpose: PURPOSE_LOGIN,
			StandardClaims: jwt.StandardClaims{
				Subject:   SUBJ_AUTH_CODE,
				Issuer:    ISSUER,
				Audience:  AUDIENCE,
				IssuedAt:  t.Unix(),
				ExpiresAt: t.Add(timeOut).Unix(),
			},
		}

	case PURPOSE_REGISTER:
		claims = &TXJWTClaim{
			Msisdn:msisdn,
			Purpose: PURPOSE_REGISTER,
			StandardClaims: jwt.StandardClaims{
				Subject:   SUBJ_AUTH_CODE,
				Issuer:    ISSUER,
				Audience:  AUDIENCE,
				IssuedAt:  t.Unix(),
				ExpiresAt: t.Add(timeOut).Unix(),
			},
		}

	case PURPOSE_RESET_PASSWORD:
		claims = &TXJWTClaim{
			Msisdn:msisdn,
			Purpose: PURPOSE_RESET_PASSWORD,
			StandardClaims: jwt.StandardClaims{
				Subject:   SUBJ_AUTH_CODE,
				Issuer:    ISSUER,
				Audience:  AUDIENCE,
				IssuedAt:  t.Unix(),
				ExpiresAt: t.Add(timeOut).Unix(),
			},
		}

	case PURPOSE_CHANGE_MSISDN:
		claims = &TXJWTClaim{
			Msisdn:msisdn,
			Purpose: PURPOSE_CHANGE_MSISDN,
			StandardClaims: jwt.StandardClaims{
				Subject:   SUBJ_AUTH_CODE,
				Issuer:    ISSUER,
				Audience:  AUDIENCE,
				IssuedAt:  t.Unix(),
				ExpiresAt: t.Add(timeOut).Unix(),
			},
		}
	}

	token, err := claimsToToken(claims, key);
	if err != nil {
		log.Panic(err.Error()) // Will send 500
	}
	return token, true
}



func refreshAuthToken(key *[]byte, token string, purpose string) (string, bool) {
	msisdn, ok := verifyAuthToken(token, key)
	if !ok || msisdn == "" {
		return "", false
	}
	return genAuthToken(key, msisdn, purpose)
}

func verifyAuthToken(ts string, key *[]byte) (string, bool) {
	token, err := jwt.ParseWithClaims(ts, &TXJWTClaim{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return *key, nil
	})

	if err != nil {
		log.Error(err.Error())
		return "", false
	}

	claims, ok := token.Claims.(*TXJWTClaim);
	if !ok || !token.Valid {
		return "", false
	}

	if claims.Subject != SUBJ_AUTH_CODE {
		return "", false
	}

	return claims.Msisdn, true
}

func verifyAuthPurpose(code string, purpose string, key *[]byte) (bool) {
	token, err := jwt.ParseWithClaims(code, &TXJWTClaim{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return *key, nil
	})

	if err != nil {
		log.Error(err.Error())
		return false
	}

	claims, ok := token.Claims.(*TXJWTClaim);
	if !ok || !token.Valid {
		return false
	}

	if claims.Subject != SUBJ_AUTH_CODE {
		return false
	}

	if claims.Purpose != purpose {
		return false
	}

	return true
}
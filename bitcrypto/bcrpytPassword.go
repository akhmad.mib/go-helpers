package bitcrypto

import (
	"golang.org/x/crypto/bcrypt"
)

const(
	COST_FACTOR = 14
)

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), COST_FACTOR)

	if err != nil {
		log.Error(err.Error())
		return "", err
	}

	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}


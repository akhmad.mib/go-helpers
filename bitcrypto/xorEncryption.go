package bitcrypto

func xorEncryptDecrypt(inputText string, xorKey string) (output string) {
	for i := range inputText {
		output += string(inputText[i] ^ xorKey[i%len(xorKey)])
	}

	return output
}
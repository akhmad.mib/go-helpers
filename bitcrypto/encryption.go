package bitcrypto

import (
	"encoding/hex"
	"fmt"
	"errors"
)

var (
	aesKey  []byte
	xorKey 	[]byte
)

func InitEncryptionKey(encAesKey []byte, encXorKey []byte) (err error) {
	aesKey = make([]byte, hex.DecodedLen(len(encAesKey)))
	_, err = hex.Decode(aesKey, encAesKey)

	xorKey = make([]byte, hex.DecodedLen(len(encXorKey)))
	_, err = hex.Decode(xorKey, encXorKey)
	if err != nil {
		return err
	}

	if len(aesKey) != 32 {
		aesKey = nil
		return fmt.Errorf("invalid AES Key's length, system should exit!")
	}

	//log.Debug("AES XOR:", string(aesKey), string(xorKey))

	return err
}


func DecryptPassword(encryptedPassword string) (string, error){
	decryptedAesPassword, err := AesDecrypt(encryptedPassword)
	if err != nil {
		return "", err
	}

	plainPassword := XorEncryptDecrypt(decryptedAesPassword) //decrypted XOR Password

	log.Debug("xor plain:", decryptedAesPassword, plainPassword)

	return plainPassword, nil
}

func AesEncrypt(plainText string) (string, error){
	if plainText == "" {
		return "", errors.New("encryption aes: plain text cannot empty")
	}

	encrypted , err := aesEncrypt(plainText, aesKey)
	if err != nil {
		log.Error(err.Error())
		return "", err
	}

	return fmt.Sprintf("%x", encrypted), nil
}

func AesDecrypt(crypt string) (string, error){
	if len(crypt) == 0 {
		return "", errors.New("encryption aes: chipertext cannot empty")
	}

	ciphertext, err := hex.DecodeString(crypt)
	if err != nil {
		log.Error(err.Error())
		return "", err
	}

	plaintext, err := aesDecrypt(ciphertext, aesKey)
	if err != nil {
		log.Error(err.Error())
		return "", err
	}

	return fmt.Sprintf("%s", plaintext), nil
}

func XorEncryptDecrypt(inputText string) (output string) {
	return xorEncryptDecrypt(inputText, string(xorKey))
}
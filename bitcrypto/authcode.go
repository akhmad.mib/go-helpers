package bitcrypto

import (
	"gitlab.com/akhmad.mib/go-helpers/bitlogging"
	"time"
	"encoding/hex"
	"fmt"
	"flag"
	"github.com/coreos/pkg/flagutil"
	"strings"
	"net/http"
)

var (
	jwtKey  []byte
	timeOut time.Duration
)

var log = bitlogging.MustGetLogger()

var clientsecret  string

func InitKey(key []byte, timeout int) (err error) {
	jwtKey = make([]byte, hex.DecodedLen(len(key)))
	_, err = hex.Decode(jwtKey, key);
	if err != nil {
		return err
	}
	if len(jwtKey) != 32 {
		jwtKey = nil
		return fmt.Errorf("invalid Key's length, system should exit!")
	}
	if timeout < 0 {
		jwtKey = nil
		return fmt.Errorf("timeout is less than zero, system should exit!")
	}
	timeOut = time.Duration(timeout) * time.Second

	err = InitSecretKey()

	return err
}

func InitSecretKey() error  {

	flags := flag.NewFlagSet("user-env", flag.PanicOnError)

	flags.StringVar(&clientsecret, "CLIENT_SECRET", "", "Client Secret User")
	err := flagutil.SetFlagsFromEnv(flags, "TIX")

	if err != nil{
		log.Error(err)
		return fmt.Errorf("Unable to get environtment variable, make sure you already set it!")
	}

	if clientsecret == ""{
		return fmt.Errorf("Unable to get environtment variable, make sure you already set it!")
	}

	log.Debug(clientsecret)

	return nil
}

func IsClientSecretValid(client string) bool  {
	return client == clientsecret
}

func GenAuthCode(msisdn string) (string, bool) {
	return genAuthToken(&jwtKey, msisdn, PURPOSE_LOGIN)
}

func GenRegisterCode(msisdn string) (string, bool) {
	return genAuthToken(&jwtKey, msisdn, PURPOSE_REGISTER)
}

func GenResetPasswordCode(msisdn string) (string, bool) {
	return genAuthToken(&jwtKey, msisdn, PURPOSE_RESET_PASSWORD)
}

func GenAnonymousCode() (string, bool)  {
	return genAuthToken(&jwtKey, clientsecret, PURPOSE_NOTLOGIN)
}

func GenChangeMsisdnCode(msisdn string) (string, bool) {
	return genAuthToken(&jwtKey, msisdn, PURPOSE_CHANGE_MSISDN)
}

func GenAuthCodeBySecretKey(clientSecret string) (string, bool)  {
	return genAuthToken(&jwtKey, clientSecret, PURPOSE_NOTLOGIN)
}

func RefreshAuthCode(token string) (string, bool) {
	return refreshAuthToken(&jwtKey, token, PURPOSE_NOTLOGIN)
}

func VerifyAuthCode(code string) (string, bool) {
	return verifyAuthToken(code, &jwtKey)
}

func VerifyPurpose(code string, purpose string) (bool) {
	return verifyAuthPurpose(code, purpose, &jwtKey)
}

func ExtractToken(r *http.Request) (string, bool) {
	token := r.Header.Get("Authorization")
	s := strings.Fields(token)
	if len(s) != 2 {
		return "", false
	}
	if s[0] != "Bearer" {
		return "", false
	}
	return s[1], true
}

func ExtractMsisdn(r *http.Request) (string, bool) {
	token, ok := ExtractToken(r)
	if !ok {
		return "", false
	}

	return verifyAuthToken(token, &jwtKey)
}
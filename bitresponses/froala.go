package bitresponses

type FroalaUpload struct {
	BaseResponse
	Link string `json:"link"`
}

type FroalaImage struct {
	Url   string `json:"url"`
	Thumb string `json:"thumb"`
}

package bitresponses

import (
	"gitlab.com/akhmad.mib/go-helpers/bitobjects"
)

type AttachmentResponse struct {
	BaseResponse
	Data    *bitobjects.Attachment
	Md5Hash string
}

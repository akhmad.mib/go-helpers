package bitresponses

type BaseResponse struct {
	Success    	bool
	Code  		int
	Message		string
}

type BaseResponseList struct {
	BaseResponse
	RecordsTotal	int
	RecordsFiltered	int
}

type Response struct {
	BaseResponse
	Data   		interface{}
}

type ResponseList struct {
	BaseResponseList
	Rows   			interface{}
}

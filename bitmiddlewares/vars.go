package bitmiddlewares

import (
	"crypto/rsa"
	"gitlab.com/akhmad.mib/go-helpers/bitlogging"
)

var log = bitlogging.MustGetLogger()

var (
	privKeyPath string = "keys/rsakey.pem"     // openssl genrsa -out app.rsa keysize
	pubKeyPath string  = "keys/rsakey.pem.pub" // openssl rsa -in app.rsa -pubout > app.rsa.pub
)

var (
	verifyKey    *rsa.PublicKey
	mySigningKey *rsa.PrivateKey
)



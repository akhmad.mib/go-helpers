package bitconfigs

type MailgunConfig struct {
	MailgunDomain       string
	MailgunApiKey       string
	MailgunPublicApiKey string
	MailgunEmailFrom    string
}


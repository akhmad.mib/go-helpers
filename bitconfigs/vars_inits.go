package bitconfigs

import (
	"gitlab.com/akhmad.mib/go-helpers/bitlogging"
	"time"
)

var log = bitlogging.MustGetLogger()

var (
	minioConfig    *MinioConfig
)

func SetMinioConfig(config *MinioConfig) (err error) {
	if minioConfig != nil {
		minioConfig = config
	}
	return
}

func GetMinioConfig()(*MinioConfig){
	return minioConfig
}

var (
	imgProxyConfig  *ImgProxyConfig
)

func SetImgProxyConfig(config *ImgProxyConfig) (err error) {
	if config != nil {
		imgProxyConfig = config
	}
	return
}

func GetImgProxyConfig()(*ImgProxyConfig){
	return imgProxyConfig
}

var (
	mailgunConfig       *MailgunConfig
)

func SetEmailMailGunConfig(config *MailgunConfig) (err error) {
	if config != nil {
		mailgunConfig = config
	}
	return
}

func GetEmailMailGunConfig()(*MailgunConfig){
	return mailgunConfig
}

var (
	keenConfig      *KeenConfig
)

func SetKeenIOConfig(config *KeenConfig) (err error){
	if config != nil {
		keenConfig = config
	}
	return
}

func GetKeenIOConfig()(*KeenConfig){
	return keenConfig
}

var (
	emailSmtpConfig *EmailSmtpConfig
)

func SetEmailSmtpConfig(config *EmailSmtpConfig) (err error) {
	if config != nil {
		emailSmtpConfig = config
	}
	return err
}

func GetEmailSmtpConfig()(*EmailSmtpConfig){
	return emailSmtpConfig
}

var (
	aesKeyString string
)

func SetAesKey(_aesKeyString string){
	aesKeyString = _aesKeyString
}

func GetAesKey()(string){
	return aesKeyString
}

var(
	timezoneLocString string
	timezoneLoc *time.Location
)

func GetTimeZoneLocationString() string{
	return timezoneLocString
}

func SetTimeZoneLocationString(_timezoneLocString string){
	timezoneLocString = _timezoneLocString
	timezoneLoc, _ = time.LoadLocation(timezoneLocString)
}

func GetTimeZoneLocation() *time.Location{
	return timezoneLoc
}

func SetTimeZoneLocation(_timezoneLoc *time.Location){
	timezoneLoc, _ = time.LoadLocation(timezoneLocString)
	timezoneLocString = timezoneLoc.String()
}

var(
	mapDataSourceConfig map[string]*DataSourceConfig
)

func GetMapDataSourceConfig()(map[string]*DataSourceConfig){
	return mapDataSourceConfig
}

func SetMapDataSourceConfig(_mapDataSourceConfig map[string]*DataSourceConfig) {
	mapDataSourceConfig = _mapDataSourceConfig
}

func GetDataSourceConfig(key string)(*DataSourceConfig){
	if mapDataSourceConfig == nil {
		return nil
	}
	return mapDataSourceConfig[key]
}

func SetDataSourceConfig(key string, config *DataSourceConfig){
	if mapDataSourceConfig == nil {
		mapDataSourceConfig = map[string]*DataSourceConfig{}
	}
	mapDataSourceConfig[key] = config
}

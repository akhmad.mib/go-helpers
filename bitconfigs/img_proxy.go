package bitconfigs

type ImgProxyConfig struct{
	ImgProxyKey 		string
	ImgProxySalt 		string
	ImgProxyUrl			string
	ImgProxyBaseUrl string
	NoImageUrl      string
}

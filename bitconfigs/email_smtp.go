package bitconfigs

type EmailSmtpConfig struct {
	EmailSender  string
	SmtpHost     string
	SmtpPort     int
	SmtpProtocol string
	SmtpUsername string
	SmtpPassword string
}

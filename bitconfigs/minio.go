package bitconfigs

type MinioConfig struct {
	MinioHost      string
	MinioUrl       string
	MinioPort      int
	MinioIsSSL     bool
	MinioRegion    string
	MinioAccessKey string
	MinioSecretKey string
}

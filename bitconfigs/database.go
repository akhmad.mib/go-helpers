package bitconfigs

import (
"time"
)

type DataSourceConfig struct {
	DbDriver         string
	DbUrl            string
	IsConnPooling    bool
	MaxIdleConns     int
	MaxOpenConns     int
	MaxConnsLifetime time.Duration
	IsLogging        bool
	DbName           string
	IsMigrate        bool
}

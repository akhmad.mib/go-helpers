package bitconfigs

type KeenConfig struct {
	KeenProjectId 		string
	KeenWriteKey 		string
}
package bitutils

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"gitlab.com/akhmad.mib/go-helpers/bitobjects"
)

func GenerateImgProxyUrl(imgProxyFile bitobjects.ImgProxyFile)(string){

	encodedURL := base64.RawURLEncoding.EncodeToString([]byte(imgProxyFile.Url))

	path := fmt.Sprintf("/%s/%d/%d/%s/%d/%s.%s", imgProxyFile.Resize, imgProxyFile.Width, imgProxyFile.Height, imgProxyFile.Gravity, imgProxyFile.Enlarge, encodedURL, imgProxyFile.Extension)

	mac := hmac.New(sha256.New, imgProxyKeyBin)
	mac.Write(imgProxySaltBin)
	mac.Write([]byte(path))
	signature := base64.RawURLEncoding.EncodeToString(mac.Sum(nil))

	return fmt.Sprintf("%s/%s%s",imgProxyUrl, signature, path)
}
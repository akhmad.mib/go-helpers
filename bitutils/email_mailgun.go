package bitutils

import (
	"fmt"
	"github.com/aymerick/douceur/inliner"
	"gitlab.com/akhmad.mib/go-helpers/bitobjects"
	"gopkg.in/mailgun/mailgun-go.v1"
)

func EmailMailgunSend(req bitobjects.Email) (err error) {
	mg := mailgun.NewMailgun(mailgunDomain, mailgunApiKey, mailgunPublicApiKey)
	for _, to := range req.To {
		message := mg.NewMessage(
			mailgunEmailFrom,
			//req.From,
			req.Subject,
			req.HTML,
			to)
		html, err := inliner.Inline(req.HTML)
		message.SetHtml(html)
		resp, id, err := mg.Send(message)
		if err != nil {
			log.Error(err)
		}
		log.Info(fmt.Sprintf("ID: %s Resp: %s\n", id, resp))
	}
	return err
}

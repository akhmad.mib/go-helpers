package bitutils

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"gitlab.com/akhmad.mib/go-helpers/bitobjects"
	"gitlab.com/akhmad.mib/go-helpers/bitrequests"
	"gopkg.in/matryer/try.v1"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path"
)

func GetHeaderRequestDefault(addHeaders map[string]string) (header http.Header) {
	header = http.Header{}
	header.Add("Content-Type", "application/json")
	header.Add("Accept", "*/*")
	header.Add("x-requested-with", "")
	for keyHeader, valueHeader := range addHeaders {
		header.Set(keyHeader, valueHeader)
	}
	return
}

func GetHeaderRequestByBearer(bearer string, addHeaders map[string]string) (header http.Header) {
	header = GetHeaderRequestDefault(addHeaders)
	header.Add("Authorization", "Bearer "+bearer)
	return
}

func GetAuthorizationRequest(url string, params map[string]string, method string, bearer string, addHeaders map[string]string) (authorization bitrequests.Authorization) {
	authorization.Url = url
	authorization.Method = method
	authorization.Params = params
	authorization.Header = GetHeaderRequestByBearer(bearer, addHeaders)
	return
}

func CurlAuthorization(authorization bitrequests.Authorization) (*http.Response, error) { //,fnCallback func(interface{},*http.Response)) (interface{},error){
	//var response *http.Response{}
	var curl *http.Request
	var err error
	var body *bytes.Buffer = nil
	if IsEmptyOrNil(body) {
		payloadBytes, err := json.Marshal(authorization.Body)
		if err != nil {
			// handle err
			return nil, err
		}
		//body := bytes.NewReader(payloadBytes)
		body = bytes.NewBuffer(payloadBytes)
		log.Info(body)
	}
	url := authorization.Url
	var i int = 0
	for key, val := range authorization.Params {
		if i == 0 {
			url += "?"
		} else {
			url += "&"
		}
		url += fmt.Sprintf("%s=%s", key, val)
		i++
	}
	log.Info(url)
	if authorization.Method != bitobjects.METHOD_GET {
		curl, err = http.NewRequest(authorization.Method, url, body)
	} else {
		curl, err = http.NewRequest(authorization.Method, url, nil)
	}
	if err != nil {
		// handle err
		return nil, err
	}
	curl.Header = authorization.Header
	//curl.Header.Add("Content-Type", "application/json")
	//curl.Header.Add("Accept","*/*")
	log.Info(curl.Header)
	responseServer, err := http.DefaultClient.Do(curl)
	log.Info(responseServer.StatusCode)
	log.Info(responseServer.Body)
	//defer responseServer.Body.Close()
	return responseServer, nil

}

func CurlDocSimpleGet(url string) (*goquery.Document, error) {
	// Request the HTML page.
	res, err := http.Get(url)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Errorf("status code error: %d %s", res.StatusCode, res.Status)
		return nil, err
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	return doc, err
}

func CurlDocSimplePost(url string, values url.Values) (*goquery.Document, error) {
	// Request the HTML page.
	res, err := http.PostForm(url, values)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Errorf("status code error: %d %s", res.StatusCode, res.Status)
		return nil, err
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	return doc, err
}

func CurlJsonSimpleGet(url string) (body []byte, err error) {
	// Request the HTML page.
	err = try.Do(func(attempt int) (bool, error) {
		log.Infof("attempt :%d", attempt)
		log.Info(url)
		req, _ := http.NewRequest("GET", url, nil)

		req.Header.Add("Cache-Control", "no-cache")
		req.Header.Add("Content-Type", "application/json")
		res, err := http.DefaultClient.Do(req)
		//res, err := http.Get(url)
		if err != nil {
			log.Info(err)
			return attempt < 2, err
		}
		defer res.Body.Close()
		if res.StatusCode != 200 {
			log.Infof("status code error: %d %s", res.StatusCode, res.Status)
			return attempt < 2, http.ErrBodyNotAllowed
		}

		body, err = ioutil.ReadAll(res.Body)
		if err != nil {
			return attempt < 2, err
		}

		body = bytes.TrimPrefix(body, []byte("\xef\xbb\xbf")) // Or []byte{239, 187, 191}
		return attempt < 2, nil
	})
	if err != nil {
		return nil, err
	}
	return body, nil
}

func CurlFormDataSimpleGet(url string) (body []byte, err error) {
	// Request the HTML page.
	err = try.Do(func(attempt int) (bool, error) {
		log.Infof("attempt :%d", attempt)
		log.Info(url)
		req, _ := http.NewRequest("GET", url, nil)

		req.Header.Add("Cache-Control", "no-cache")
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		res, err := http.DefaultClient.Do(req)
		//res, err := http.Get(url)
		if err != nil {
			log.Info(err)
			return attempt < 2, err
		}
		defer res.Body.Close()
		if res.StatusCode != 200 {
			log.Infof("status code error: %d %s", res.StatusCode, res.Status)
			return attempt < 2, http.ErrBodyNotAllowed
		}

		body, err = ioutil.ReadAll(res.Body)
		if err != nil {
			return attempt < 2, err
		}

		body = bytes.TrimPrefix(body, []byte("\xef\xbb\xbf")) // Or []byte{239, 187, 191}
		return attempt < 2, nil
	})
	if err != nil {
		return nil, err
	}
	return body, nil
}

func CurlJsonSimpleGetWithParams(url string, params url.Values) (body []byte, err error) {
	// Request the HTML page.
	err = try.Do(func(attempt int) (bool, error) {
		log.Infof("attempt :%d", attempt)
		log.Info(url)
		req, _ := http.NewRequest("GET", url, nil)

		req.Header.Add("Cache-Control", "no-cache")
		req.URL.RawQuery = params.Encode()

		res, err := http.DefaultClient.Do(req)
		//res, err := http.Get(url)
		if err != nil {
			log.Info(err)
			return attempt < 2, err
		}
		defer res.Body.Close()
		if res.StatusCode != 200 {
			log.Infof("status code error: %d %s", res.StatusCode, res.Status)
			return attempt < 2, http.ErrBodyNotAllowed
		}

		body, err = ioutil.ReadAll(res.Body)
		if err != nil {
			return attempt < 2, err
		}

		body = bytes.TrimPrefix(body, []byte("\xef\xbb\xbf")) // Or []byte{239, 187, 191}
		return attempt < 2, nil
	})
	if err != nil {
		return nil, err
	}
	return body, nil
}

func CurlJsonSimplePost(url string, values url.Values) (body []byte, err error) {
	// Request the HTML page.
	err = try.Do(func(attempt int) (bool, error) {
		log.Infof("attempt :%d", attempt)
		res, err := http.PostForm(url, values)
		if err != nil {
			log.Error(err)
			return attempt < 2, err
		}
		defer res.Body.Close()
		if res.StatusCode != 200 {
			log.Infof("status code error: %d %s", res.StatusCode, res.Status)
			return attempt < 2, http.ErrBodyNotAllowed
		}

		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return attempt < 2, err
		}
		body = bytes.TrimPrefix(body, []byte("\xef\xbb\xbf")) // Or []byte{239, 187, 191}

		return attempt < 2, nil
	})
	if err != nil {
		return nil, err
	}
	return body, nil
}

func CurlGojekGet(endpoint string, qs map[string]string) (body []byte, err error) { //,fnCallback func(interface{},*http.Response)) (interface{},error){
	//var response *http.Response{}
	var curl *http.Request
	var baseUrl string = "https://api.gojekapi.com"
	var url string
	url = baseUrl + endpoint
	var i int = 0
	for key, val := range qs {
		if i == 0 {
			url += "?"
		} else {
			url += "&"
		}
		url += fmt.Sprintf("%s=%s", key, val)
		i++
	}
	curl, err = http.NewRequest("GET", url, nil)
	curl.Header.Add("Content-Type", "application/json")
	curl.Header.Add("X-AppVersion", "3.10.0")
	curl.Header.Add("X-UniqueId", "3454d56d682e549c")
	curl.Header.Add("Authorization", "Bearer e90d014f-112f-4294-a7cd-fa22d46eff55")
	log.Info(curl.Header)
	res, err := http.DefaultClient.Do(curl)
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		log.Info(err)
	}
	body = bytes.TrimPrefix(body, []byte("\xef\xbb\xbf"))
	//defer responseServer.Body.Close()
	return

}

func DownloadFileToTemp(url string) (pathFile string, err error) {
	// Request the HTML page.
	var name = path.Base(url)
	log.Info(name)
	dir := "."
	relativeDir := "/static/temporary/uploads/"
	relPath := relativeDir + name
	pathFile = dir + relPath

	res, err := http.Get(url)
	if err != nil {
		log.Error(err)
		return
	}
	if res.StatusCode != 200 {
		log.Errorf("status code error: %d %s", res.StatusCode, res.Status)
		return
	}
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Error(err)
		return
	}
	err = ioutil.WriteFile(pathFile, data, 0666)
	if err != nil {
		log.Error(err)
		return
	}
	return
}

func ReadCSVFromUrl(url string, delimiter int32) ([][]string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	reader := csv.NewReader(resp.Body)
	reader.Comma = delimiter
	data, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	return data, nil
}

func UrlGetContent(url string) (string, error) {
	res, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	byteArrayContent, err := ioutil.ReadAll(res.Body)
	log.Info(err)
	res.Body.Close()
	return string(byteArrayContent[:]), err
}

func ReadCSVFromPath(path string, delimiter int32) ([][]string, error) {
	csvFile, _ := os.Open(path)
	reader := csv.NewReader(bufio.NewReader(csvFile))
	reader.Comma = delimiter
	data, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	return data, nil
}

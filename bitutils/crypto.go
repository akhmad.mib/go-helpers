package bitutils

import (
	"crypto/aes"
	"crypto/cipher"
	"bytes"
	"fmt"
	"encoding/hex"
	"errors"
	"crypto/hmac"
	"crypto/sha256"
	"os"
	"io"
)

func AesEncrypt(plainText string) (string, error){
	if plainText == "" {
		return "", errors.New("encryption aes: plain text cannot empty")
	}

	encrypted , err := aesEncrypt(plainText, aesKey)
	if err != nil {
		log.Error(err.Error())
		return "", err
	}

	return fmt.Sprintf("%x", encrypted), nil
}

func AesDecrypt(crypt string) (string, error){
	if len(crypt) == 0 {
		return "", errors.New("encryption aes: chipertext cannot empty")
	}

	ciphertext, err := hex.DecodeString(crypt)
	if err != nil {
		log.Error(err.Error())
		return "", err
	}

	plaintext, err := aesDecrypt(ciphertext, aesKey)
	if err != nil {
		log.Error(err.Error())
		return "", err
	}

	return fmt.Sprintf("%s", plaintext), nil
}


func aesEncrypt(src string, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	ecb := cipher.NewCBCEncrypter(block, []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0})
	content := []byte(src)
	content = pkcs5Padding(content, block.BlockSize())
	crypted := make([]byte, len(content))
	ecb.CryptBlocks(crypted, content)

	return crypted, nil
}

func aesDecrypt(crypt []byte, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	ecb := cipher.NewCBCDecrypter(block, []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0})
	decrypted := make([]byte, len(crypt))
	ecb.CryptBlocks(decrypted, crypt)

	return pkcs5Trimming(decrypted), nil
}

func pkcs5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func pkcs5Trimming(encrypt []byte) []byte {
	padding := encrypt[len(encrypt)-1]
	return encrypt[:len(encrypt)-int(padding)]
}

func ComputeHmac256(message string, secret string) string {
	key := []byte(secret)
	h := hmac.New(sha256.New, key)
	h.Write([]byte(message))
	//return base64.URLEncoding.EncodeToString(h.Sum(nil))
	//return string(h.Sum(nil))
	return hex.EncodeToString(h.Sum(nil))
}

func HashFileMD5(filePath string) (string, error) {
	//Initialize variable returnMD5String now in case an error has to be returned
	var returnMD5String string

	//Open the passed argument and check for any error
	file, err := os.Open(filePath)
	if err != nil {
		return returnMD5String, err
	}

	//Tell the program to call the following function when the current function returns
	defer file.Close()

	//Open a new hash interface to write to
	//hash := md5.New()
	hash := sha256.New()

	//Copy the file in the hash interface and check for any error
	if _, err := io.Copy(hash, file); err != nil {
		return returnMD5String, err
	}

	//Get the 16 bytes hash
	//hashInBytes := hash.Sum(nil)[:16]
	hashInBytes := hash.Sum(nil)

	//Convert the bytes to a string
	returnMD5String = hex.EncodeToString(hashInBytes)

	return returnMD5String, nil

}
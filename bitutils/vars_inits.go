package bitutils

import (
	"encoding/hex"
	"fmt"
	"github.com/bwmarrin/snowflake"
	"gitlab.com/akhmad.mib/go-helpers/bitconfigs"
	"gitlab.com/akhmad.mib/go-helpers/bitlogging"
	"gopkg.in/inconshreveable/go-keen.v0"
	"strconv"
	"time"
)

var log = bitlogging.MustGetLogger()

var (
	minioHost      string
	minioUrl       string
	minioPort      int
	minioIsSSL     bool
	minioRegion    string
	minioAccessKey string
	minioSecretKey string

	//minioConfig    *configs.MinioConfig
)

func InitMinioConfig(config *bitconfigs.MinioConfig) (err error) {
	if config != nil {
		minioHost = config.MinioHost
		minioUrl = config.MinioUrl
		minioPort = config.MinioPort
		minioIsSSL = config.MinioIsSSL
		minioRegion = config.MinioRegion
		minioAccessKey = config.MinioAccessKey
		minioSecretKey = config.MinioSecretKey

		//minioConfig = config
	}
	return
}

func InitMinioFromConfig(){
	InitMinioConfig(bitconfigs.GetMinioConfig())
}

var (
	imgProxyKey     string
	imgProxySalt    string
	imgProxyUrl     string
	imgProxyKeyBin  []byte
	imgProxySaltBin []byte
	imgProxyBaseUrl string
	noImageUrl      string

	//imgProxyConfig  *configs.ImgProxyConfig
)

func InitImgProxyConfig(config *bitconfigs.ImgProxyConfig) (err error) {
	if config != nil {
		imgProxyKey     = config.ImgProxyKey
		imgProxySalt    = config.ImgProxySalt
		imgProxyUrl     = config.ImgProxyUrl
		if imgProxyKeyBin, err = hex.DecodeString(imgProxyKey); err != nil {
			log.Errorln("Key expected to be hex-encoded string")
		}

		if imgProxySaltBin, err = hex.DecodeString(imgProxySalt); err != nil {
			log.Errorln("Salt expected to be hex-encoded string")
		}
		imgProxyBaseUrl = config.ImgProxyBaseUrl
		noImageUrl = config.NoImageUrl

		//imgProxyConfig = config
	}
	return
}

func InitImgProxyFromConfig(){
	InitImgProxyConfig(bitconfigs.GetImgProxyConfig())
}

var (
	mailgunDomain       string
	mailgunApiKey       string
	mailgunPublicApiKey string
	mailgunEmailFrom    string

	//mailgunConfig       *configs.MailgunConfig
)

func InitEmailMailGunConfig(config *bitconfigs.MailgunConfig) (err error) {
	if config != nil {
		mailgunDomain = config.MailgunDomain
		mailgunApiKey = config.MailgunApiKey
		mailgunPublicApiKey = config.MailgunPublicApiKey
		mailgunEmailFrom = config.MailgunEmailFrom
		log.Info(fmt.Sprintf("mailgunDomain=%v ; mailgunApiKey=%v ; mailgunPublicApiKey=%v", mailgunDomain, mailgunApiKey, mailgunPublicApiKey))

		//mailgunConfig = config
	}
	return
}

func InitEmailMailGunFromConfig(){
	InitEmailMailGunConfig(bitconfigs.GetEmailMailGunConfig())
}

var (
	keenProjectId   string
	keenWriteKey    string
	keenClient      *keen.Client
	keenBatchClient *keen.BatchClient

	//keenConfig      *configs.KeenConfig
)

func InitKeenIOConfig(config *bitconfigs.KeenConfig) (err error){
	if config != nil {
		keenProjectId = config.KeenProjectId
		keenWriteKey = config.KeenWriteKey
		keenClient = &keen.Client{ApiKey: keenWriteKey, ProjectToken: keenProjectId}
		keenBatchClient = keen.NewBatchClient(keenClient, keenFlushInterval)

		//keenConfig = config
	}
	return
}

func InitKeenIOFromConfig(){
	InitKeenIOConfig(bitconfigs.GetKeenIOConfig())
}

var (
	emailSender     string
	smtpHost        string
	smtpPort        int
	smtpProtocol    string
	smtpUsername    string
	smtpPassword    string

	//emailSmtpConfig *configs.EmailSmtpConfig
)

func InitEmailSmtpConfig(config *bitconfigs.EmailSmtpConfig) (err error) {
	if config != nil {
		emailSender = config.EmailSender
		smtpHost = config.SmtpHost
		smtpPort = config.SmtpPort
		smtpProtocol = config.SmtpProtocol
		smtpUsername = config.SmtpUsername
		smtpPassword = config.SmtpPassword
		log.Info(fmt.Sprintf("smtpHost=%v ; smtpPort=%v ; smtpProtocol=%v ; smtpUsername=%v ; smtpPassword=%v", smtpHost, smtpPort, smtpProtocol, smtpUsername, smtpPassword))

		//emailSmtpConfig = config
	}
	return err
}

func InitEmailSmtpFromConfig(){
	InitEmailSmtpConfig(bitconfigs.GetEmailSmtpConfig())
}


var (
	aesKey []byte
)

func InitAesKey(aesKeyString string){
	aesKey = []byte(aesKeyString)
}

func InitAesKeyFromConfig(){
	InitAesKey(bitconfigs.GetAesKey())
}

var (
	timezoneLoc *time.Location
)

func InitTimeZoneLocationFromConfig(){
	//timezoneLocString := time.Now().Location().String()
	//SetTimeZoneLocation(timezoneLocString)

	if bitconfigs.GetTimeZoneLocation() == nil {
		timezoneLocString := time.Now().Location().String()
		timezoneLoc, _ := time.LoadLocation(timezoneLocString)
		bitconfigs.SetTimeZoneLocation(timezoneLoc)
	}
	SetTimeZoneLocation(bitconfigs.GetTimeZoneLocation())
}

func GetTimeZoneLocation() *time.Location{
	return timezoneLoc
}

func SetTimeZoneLocation(_timezoneLoc *time.Location){
	//timezoneLoc, _ = time.LoadLocation(timezoneLocString)
	timezoneLoc = _timezoneLoc
}

var node *snowflake.Node

// InitSnowflake initiate Snowflake node singleton.
func InitSnowflake() error {
	// Get node number from env TIX_NODE_NO
	key := "1"
	// Parse node number
	nodeNo, err := strconv.ParseInt(key, 10, 64)
	if err != nil {
		return err
	}
	// Create snowflake node
	n, err := snowflake.NewNode(nodeNo)
	if err != nil {
		return err
	}
	// Set node
	node = n
	return nil
}

func InitAllByConfigs(){
	InitTimeZoneLocationFromConfig()
	InitAesKeyFromConfig()
	InitSnowflake()
	InitMinioFromConfig()
	InitEmailSmtpFromConfig()
	InitEmailMailGunFromConfig()
	InitImgProxyFromConfig()
	InitKeenIOFromConfig()
}
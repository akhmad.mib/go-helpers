package bitutils

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"github.com/leekchan/accounting"
	"github.com/microcosm-cc/bluemonday"
	"gitlab.com/akhmad.mib/go-helpers/bitobjects"
	"html/template"
	"math/big"
	"net/url"
	"reflect"
	"strconv"
	"strings"
)


func GetAllFuncMap() (template.FuncMap){
	return template.FuncMap{
		"selectCombobox" : selectCombobox,
		"selectMultipleCombobox" : selectMultipleCombobox,
		"selectComboboxArray" : selectComboboxArray,
		"setActiveMenu" : setActiveMenu,
		"selectCheckboxStruct": selectCheckboxStruct,
		"displayMoney" : displayMoney,
		"displayDefaultMoney" : displayDefaultMoney,
		"showUrlAttachment":showUrlAttachment,
		"showUrlAttachmentFirst":showUrlAttachmentFirst,
		"showImageUrl":showImageUrl,
		"showImageBaseUrl":showImageBaseUrl,
		"listStructToStringDelimited" : listStructToStringDelimited,
		"getGravatar" : getGravatar,
		"cleanHTML":cleanHTML,
		"scanIframe":scanIframe,
		"setVarsMap":setVarsMap,
	}
}

func listStructToStringDelimited(list interface{},targetLabel string, delimiter string) template.HTML {
	stringDelimited := ""
	listData := reflect.ValueOf(list)
	log.Info(listData.Len())
	for i := 0; i < listData.Len(); i++ {
		val := listData.Index(i)
		valueOption := ""
		log.Info(val.NumField())
		for x := 0; x < val.NumField(); x++ {
			valueField := val.Field(x)
			typeField := val.Type().Field(x)
			if typeField.Name == targetLabel {
				valueOption = fmt.Sprintf("%v",valueField.Interface())
			}
		}
		if i>0 {
			stringDelimited += ","
		}
		stringDelimited += fmt.Sprintf("%v", valueOption)
	}
	return template.HTML(stringDelimited)
}

func showUrlAttachment(attachment bitobjects.Attachment) template.HTML {
	imageBaseUrl := getMinioBaseUrl()
	//minioIsSSL := configs.GetConfigBool("minio_is_ssl")
	minioProtocol := "http"
	if minioIsSSL {
		minioProtocol = "https"
	}
	var minioFullPath string = fmt.Sprintf("%s://%v/%v/%v",minioProtocol, imageBaseUrl, attachment.BucketName, attachment.PathFile)
	return template.HTML(minioFullPath)
}

func ShowUrlAttachment(attachment bitobjects.Attachment) string {
	return fmt.Sprintf("%v",showUrlAttachment(attachment))
}

func showUrlAttachmentFirst(attachments []bitobjects.Attachment) template.HTML {
	//minioBaseUrl := getMinioBaseUrl()
	//minioFullPath := fmt.Sprintf("//%v/%v/%v",minioBaseUrl,attachment.BucketName,attachment.PathFile)
	if len(attachments) > 0 {
		attachment := attachments[0]
		//minioFullPath := fmt.Sprintf("//ce8a3f4dc.cloudimg.io/cdn/n/n/%v/%v/%v", minioBaseUrl, attachment.BucketName, attachment.PathFile)
		return showUrlAttachment(attachment)
	}else{
		//noImagePath := "//"+configs.GetConfigString("no_image_url")
		noImagePath := "//"+noImageUrl
		return template.HTML(noImagePath)
	}
}

func showImageBaseUrl() template.HTML {
	return template.HTML(getImageBaseUrl())
}

func getImageBaseUrl() string {
	//minioHost := configs.GetConfigString("minio_host")
	//imageProxyBaseUrl := configs.GetConfigString("image_proxy_base_url")
	var minioFullPath string = getMinioBaseUrl()
	if minioHost == "127.0.0.1" || true{
		minioFullPath = fmt.Sprintf("//%v", minioFullPath)
	}else {
		minioFullPath = fmt.Sprintf("//%v/%v",imgProxyBaseUrl,minioFullPath)
	}
	return minioFullPath
}

func getMinioBaseUrl() string {
	//minioUrl := configs.GetConfigString("minio_url")
	//minioHost := configs.GetConfigString("minio_host")
	//minioPort := configs.GetConfigInt("minio_port")
	minioBaseUrl := ""
	if minioPort == 80 {
		//minioBaseUrl = minioHost
		minioBaseUrl = minioUrl
	}else{
		//minioBaseUrl = fmt.Sprintf("%v:%v",minioHost,minioPort)
		minioBaseUrl = minioUrl
	}
	//minioFullPath := fmt.Sprintf("//%v/%v/%v",minioBaseUrl,attachment.BucketName,attachment.PathFile)
	//minioFullPath := fmt.Sprintf("http://%v",minioBaseUrl)
	minioFullPath := fmt.Sprintf("%v",minioBaseUrl)
	return minioFullPath
}

func showImageUrl(BucketName string, PathFile string) template.HTML {
	minioBaseUrl := getMinioBaseUrl()
	//minioFullPath := fmt.Sprintf("//%v/%v/%v",minioBaseUrl,attachment.BucketName,attachment.PathFile)
	minioFullPath := fmt.Sprintf("//%v/%v/%v",minioBaseUrl,BucketName,PathFile)
	return template.HTML(minioFullPath)
}


func displayDefaultMoney(money interface{}) template.HTML {
	return displayMoney(money,"Rp.",0,".",",")
}

func displayMoney(money interface{},symbol string, precision int,thousand string, decimal string) template.HTML {
	val := ""
	var moneyFloat float64
	moneyStr := fmt.Sprintf("%v",money)
	//log.Info("displayMoney")
	//log.Info(moneyStr)
	moneyFloat,_ = strconv.ParseFloat(moneyStr, 64);
	bigVal :=  big.NewFloat(moneyFloat)//new(big.Float).SetPrec(2).SetString(moneyStr)
	//log.Info(bigVal)
	ac := accounting.Accounting{Symbol: symbol, Precision: precision, Thousand: thousand, Decimal: decimal}
	moneyFloat,_ = bigVal.Float64()
	log.Info(moneyFloat)
	val = ac.FormatMoney(moneyFloat)
	//log.Info(val)
	//log.Info("end displayMoney")
	return template.HTML(val)
}

func selectCombobox(list interface{},valueCombobox string, labelCombobox string, valueData interface{}) template.HTML {
	comboboxOptions := ""
	listData := reflect.ValueOf(list)
	log.Info(listData.Len())
	for i := 0; i < listData.Len(); i++ {
		val := listData.Index(i)
		labelOption := ""
		valueOption := ""
		valueCompareOption := fmt.Sprintf("%v",valueData)
		selectedOption := ""
		log.Info(val.NumField())
		for x := 0; x < val.NumField(); x++ {
			valueField := val.Field(x)
			typeField := val.Type().Field(x)
			if typeField.Name == labelCombobox {
				labelOption = fmt.Sprintf("%v",valueField.Interface())
			}
			if typeField.Name == valueCombobox {
				valueOption = fmt.Sprintf("%v",valueField.Interface())
			}
		}
		if valueOption == valueCompareOption {
			selectedOption = " selected='selected' "
		}
		comboboxOptions += fmt.Sprintf("<option value='%v' %v>%v</option>\n", valueOption, selectedOption, labelOption)
	}
	return template.HTML(comboboxOptions)
}

func selectMultipleCombobox(list interface{},valueCombobox string, labelCombobox string, listData interface{}, valueDataCombobox string) template.HTML {
	comboboxOptions := ""
	listMaster := reflect.ValueOf(list)
	listValueData := reflect.ValueOf(listData)
	log.Info(listMaster.Len())
	for i := 0; i < listMaster.Len(); i++ {
		labelOption := ""
		valueOption := ""
		valueCompareOption := ""
		selectedOption := ""
		val := listMaster.Index(i)
		log.Info(val.NumField())
		for x := 0; x < val.NumField(); x++ {
			valueField := val.Field(x)
			typeField := val.Type().Field(x)
			if typeField.Name == labelCombobox {
				labelOption = fmt.Sprintf("%v", valueField.Interface())
			}
			if typeField.Name == valueCombobox {
				valueOption = fmt.Sprintf("%v", valueField.Interface())
			}
		}
		for z := 0; z < listValueData.Len(); z++ {
			val := listValueData.Index(z)
			log.Info(val.NumField())
			for x := 0; x < val.NumField(); x++ {
				valueField := val.Field(x)
				typeField := val.Type().Field(x)
				if typeField.Name == valueDataCombobox {
					valueCompareOption = fmt.Sprintf("%v", valueField.Interface())
				}
			}
			if valueOption == valueCompareOption {
				selectedOption = " selected='selected' "
			}
		}

		comboboxOptions += fmt.Sprintf("<option value='%v' %v>%v</option>\n", valueOption, selectedOption, labelOption)
	}
	return template.HTML(comboboxOptions)
}

func selectComboboxArray(list interface{}, valueData interface{}) template.HTML {
	comboboxOptions := ""
	listData := reflect.ValueOf(list)
	log.Info(listData.Len())
	for i := 0; i < listData.Len(); i++ {
		val := listData.Index(i)

		labelOption := fmt.Sprintf("%v",val.Interface())
		valueOption := fmt.Sprintf("%v",val.Interface())
		valueCompareOption := fmt.Sprintf("%v",valueData)
		selectedOption := ""
		if valueOption == valueCompareOption {
			selectedOption = " selected='selected' "
		}
		comboboxOptions += fmt.Sprintf("<option value='%v' %v>%v</option>\n", valueOption, selectedOption, labelOption)
	}
	return template.HTML(comboboxOptions)
}

func selectCheckboxStruct(varName string, listMaster interface{}, fieldValueMaster string, fieldLabelMaster string, listData interface{}, fieldData string) template.HTML {
	checkbox := ""
	listMasterValueOf := reflect.ValueOf(listMaster)
	listDataValueOf := reflect.ValueOf(listData)
	log.Info(listMasterValueOf.Len())
	for im := 0; im < listMasterValueOf.Len(); im++ {
		valMaster := listMasterValueOf.Index(im)
		labelText := ""
		valueText := ""
		valueCompareMaster := ""
		valueCompareData := ""
		checkedAttribute := ""
		log.Info(valMaster.NumField())
		for xm := 0; xm < valMaster.NumField(); xm++ {
			valueField := valMaster.Field(xm)
			typeField := valMaster.Type().Field(xm)
			if typeField.Name == fieldValueMaster {
				valueCompareMaster = fmt.Sprintf("%v",valueField.Interface())
				valueText = valueCompareMaster
			}
			if typeField.Name == fieldLabelMaster {
				labelText = fmt.Sprintf("%v",valueField.Interface())
			}
		}
		for id := 0; id < listDataValueOf.Len(); id++ {
			valData := listDataValueOf.Index(id)
			log.Info(valData.NumField())
			for xd := 0; xd < valData.NumField(); xd++ {
				valueField := valData.Field(xd)
				typeField := valData.Type().Field(xd)
				if typeField.Name == fieldData {
					valueCompareData = fmt.Sprintf("%v",valueField.Interface())
				}
			}
			if valueCompareMaster == valueCompareData {
				checkedAttribute = " checked='checked' "
			}
		}

		checkbox += fmt.Sprintf("<input class='form%v' id='form%v-%v' type='checkbox' name='%v[]' value='%v' %v> %v\n", varName,varName,valueText,varName,valueText,checkedAttribute, labelText)
	}
	return template.HTML(checkbox)
}

func setActiveMenu(pathUrl string, menuUrl string) template.HTML {
	val := ""
	if strings.Contains(pathUrl,menuUrl) {
		val = "active"
	}
	return  template.HTML(val)
}

func getGravatar(email string) template.HTML {
	hasher := md5.New()
	hasher.Write([]byte(email))
	avatar := hex.EncodeToString(hasher.Sum(nil))
	avatar = strings.TrimSpace(avatar)
	avatar = strings.ToLower(avatar)
	defaultAvatar := "https://ce8a3f4dc.cloudimg.io/width/55/s/cdn.woorkup.com/wp-content/uploads/2016/04/gravatar.png"
	defaultAvatar = url.QueryEscape(defaultAvatar)
	size := 55
	avatarUrl := fmt.Sprintf("https://www.gravatar.com/avatar/%s?d=%s&s=%v",avatar,defaultAvatar,size)
	return template.HTML(avatarUrl)
}

func cleanHTML(content string) template.HTML{
	p := bluemonday.StrictPolicy()
	return template.HTML(p.Sanitize(content))
}

func scanIframe(content string) template.HTML{
	return template.HTML(HtmlFilterIframe(content))
}

func setVarsMap(m map[interface{}]interface{},kvs ...interface{}) (map[interface{}]interface{}) {
	if len(kvs)%2 != 0 {
		return nil
	}
	for i := 0; i < len(kvs); i += 2 {
		s, ok := kvs[i].(string)
		if !ok {
			return nil
		}
		m[s] = kvs[i+1]
	}
	return m
}

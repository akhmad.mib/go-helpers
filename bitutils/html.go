package bitutils

import (
	"bytes"
	"gitlab.com/akhmad.mib/go-helpers/bitobjects"
	"io"
	"golang.org/x/net/html"
	"strings"
)

func scanAllImage(doc *html.Node) (*html.Node) {
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "img" {
			var attrs []html.Attribute
			excludeAttrs := []string{"on","style"}
			for _,value := range n.Attr{
				isValid := true
				for _,excAttr := range excludeAttrs{
					if strings.HasPrefix(strings.ToLower(value.Key), excAttr){
						isValid = false
					}
				}

				if strings.ToLower(value.Key) == "src" {
					var imgProxyFile bitobjects.ImgProxyFile
					imgProxyFile.Resize = "fit"
					imgProxyFile.Url = value.Val
					imgProxyFile.Width = 500
					imgProxyFile.Height = 500
					imgProxyFile.Gravity = "no"
					imgProxyFile.Enlarge = 1
					imgProxyFile.Extension = "png"

					value.Val = GenerateImgProxyUrl(imgProxyFile)
				}

				if isValid {
					attrs = append(attrs,value)
				}
			}
			n.Attr = attrs
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	return doc
}

func scanAllIframe(doc *html.Node) (*html.Node) {
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "iframe" {
			var attrs []html.Attribute
			excludeAttrs := []string{"on","style"}
			isFoundClass := false;
			for _,value := range n.Attr{
				isValid := true
				for _,excAttr := range excludeAttrs{
					if strings.HasPrefix(strings.ToLower(value.Key), excAttr){
						isValid = false
					}
				}
				if strings.ToLower(value.Key) == "class" {
					isFoundClass = true
					value.Val = value.Val+" iframe-youtube";
				}

				if isValid {
					attrs = append(attrs,value)
				}
			}
			if !isFoundClass {
				attr := html.Attribute{}
				attr.Key = "class"
				attr.Val = "iframe-youtube"
				attrs = append(attrs,attr)
			}
			n.Attr = attrs
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	return doc
}

func renderNode(n *html.Node) string {
	var buf bytes.Buffer
	w := io.Writer(&buf)
	html.Render(w, n)
	return buf.String()
}

func HtmlFilterImageXss(htm string)(string){
	doc, _ := html.Parse(strings.NewReader(htm))
	return renderNode(scanAllImage(doc))
}

func HtmlFilterIframe(htm string)(string){
	doc, _ := html.Parse(strings.NewReader(htm))
	return renderNode(scanAllIframe(doc))
}
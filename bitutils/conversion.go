package bitutils

import "strconv"

func StringToFloat32(str string) float32 {
	value, err := strconv.ParseFloat(str, 32)
	if err != nil {
		// do something sensible
		return float32(0)
	}
	return float32(value)
}

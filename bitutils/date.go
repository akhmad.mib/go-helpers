package bitutils

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

func IsNowDate(date string) bool {
	t, err := time.Parse(time.RFC3339, date)

	if err != nil {
		log.Info(err)
	}

	timeOne := t.Truncate(24 * time.Hour)
	timeTwo := GetTimeNow().Truncate(24 * time.Hour)

	return timeOne.Equal(timeTwo)
}

func IsSameDate(date string, dateTwo string) bool {
	t, err := time.Parse(time.RFC3339, date)
	t2, err := time.Parse(time.RFC3339, dateTwo)

	if err != nil {
		log.Info(err)
	}

	timeOne := t.Truncate(24 * time.Hour)
	timeTwo := t2.Truncate(24 * time.Hour)

	return timeOne.Equal(timeTwo)
}

func IsSameTime(date string, dateTwo string) bool {
	t, err := time.ParseInLocation("2006-01-02 15:04:05", date, timezoneLoc)
	t2, err := time.ParseInLocation("2006-01-02 15:04:05", dateTwo, timezoneLoc)

	if err != nil {
		log.Error(err)
	}

	timeOne := t.Truncate(24 * time.Hour)
	timeTwo := t2.Truncate(24 * time.Hour)

	return timeOne.Equal(timeTwo)
}

func ToTimeRFC3339(unixTime int64) string {
	unixTimeUTC := time.Unix(unixTime, 0)
	return unixTimeUTC.Format(time.RFC3339)
}

func StringToTime(unixTime string) time.Time {
	i, err := strconv.ParseInt(unixTime, 10, 64)
	if err != nil {
		panic(err)
	}
	tm := time.Unix(i, 0)
	return tm
}

func StringToTimeZone(unixTime string, timezoneLocString string) time.Time {
	timezoneLoc, _ := time.LoadLocation(timezoneLocString)
	i, err := strconv.ParseInt(unixTime, 10, 64)
	if err != nil {
		panic(err)
	}
	tm := time.Unix(i, 0).In(timezoneLoc)
	return tm
}

func Int64ToTime(unixTime int64) time.Time {
	tm := time.Unix(unixTime, 0)
	return tm
}

func Int64ToTimeZone(unixTime int64, timezoneLocString string) time.Time {
	timezoneLoc, _ := time.LoadLocation(timezoneLocString)
	tm := time.Unix(unixTime, 0).In(timezoneLoc)
	return tm
}

func ToEpoch(date string) int64 {
	t, err := time.Parse(time.RFC3339, date)
	if err != nil {
		log.Error(err)
	}
	return t.Unix()
}

func ToEpochWIthTruncate(date string) int64 {
	t, err := time.Parse(time.RFC3339, date)
	if err != nil {
		log.Error(err)
	}
	timeTruncate := t.Truncate(24 * time.Hour)
	return timeTruncate.Unix()
}

func ToTimeDate(date string) (resDate string, resTime string) {
	t, err := time.Parse(time.RFC3339, date)
	if err != nil {
		log.Error(err)
	}

	hour, min, _ := t.Clock()

	year, month, day := t.Date()

	resTm := fmt.Sprintf("%02d:%02d", hour, min)
	resDt := fmt.Sprintf("%02d-%02d-%d", day, month, year)

	return resDt, resTm
}

func ConvertDateAndTimeStringToTime(dateStr string, timeStr string) time.Time{
	dateTime := dateStr+" "+timeStr+""
	//log.Info(dateTime)
	t, err := time.ParseInLocation("02-01-2006 15:04:05", dateTime,timezoneLoc)
	//log.Info(t)
	if err != nil {
		log.Error(err)
	}
	return t
}

func TimeParse(format,dateTime string) (time.Time, error) {
	//dateTime := dateStr+" "+timeStr+""
	//log.Info(dateTime)
	t, err := time.ParseInLocation(format, dateTime,timezoneLoc)
	//log.Info(t)
	//if err != nil {
	//	log.Error(err)
	//}
	return t, err
}

func GetTimeNow() time.Time{
	//init the loc
	//set timezone,
	now := time.Now().In(timezoneLoc)
	return now
}

func AddTime(dateTime time.Time,d time.Duration) time.Time{
	return dateTime.Add(d)
}

func ToTimeString(time time.Time) (resDate string) {
	return time.In(timezoneLoc).Format("2006-01-02 15:04:05")
}

func ToTime(timeStr string) (time.Time) {
	t, err := time.ParseInLocation("2006-01-02 15:04:05", timeStr,timezoneLoc)
	//log.Info(t)
	if err != nil {
		log.Error(err)
	}
	return t
}

func IsValildTime(timeStr string) (bool) {
	_, err := time.ParseInLocation("2006-01-02 15:04:05", timeStr,timezoneLoc)
	//log.Info(t)
	if err != nil {
		return false
	}
	return true
}

func ToTimeRFC3339String(date time.Time) (resDate string) {
	return date.Format(time.RFC3339)
}

func GetYesterday() (timeYesterday time.Time) {
	timeStrYesterday := GetTimeNow().Add(-24*time.Hour).Format("2006-01-02 15:04:05")
	t, err := time.Parse("2006-01-02 15:04:05", timeStrYesterday)
	if err != nil {
		log.Error(err)
	}
	return t
}

func GetBeginningTimeByAddDays(days int64) (timeYesterday time.Time) {
	timeStrBeginningYesterday := GetTimeNow().Add(time.Duration(days)*24*time.Hour).Format("2006-01-02 00:00:00")
	t, err := time.Parse("2006-01-02 00:00:00", timeStrBeginningYesterday)
	if err != nil {
		log.Error(err)
	}
	return t
}

func GetTimeByAddDays(days int64) (timeYesterday time.Time) {
	timeStrBeginningYesterday := GetTimeNow().Add(time.Duration(days)*24*time.Hour).Format("2006-01-02 15:04:05")
	t, err := time.Parse("2006-01-02 15:04:05", timeStrBeginningYesterday)
	if err != nil {
		log.Error(err)
	}
	return t
}

func zip(a1, a2 []string) []string {
	r := make([]string, 2*len(a1))
	for i, e := range a1 {
		r[i*2] = e
		r[i*2+1] = a2[i]
	}
	return r
}

func GetGolangTimeFormatFromPHPTimeFormat(phpFormat string) string {
	array1 := []string{"Y", "m","d","H","i","s"}
	array2 := []string{"2006", "01","02","15","04","05"}
	golangFormat := strings.NewReplacer(zip(array1, array2)...).Replace(phpFormat)
	return golangFormat
}

func GetBeginningYesterday() (timeYesterday time.Time) {
	timeStrBeginningYesterday := GetTimeNow().Add(-24*time.Hour).Format("2006-01-02 00:00:00")
	t, err := time.Parse("2006-01-02 00:00:00", timeStrBeginningYesterday)
	if err != nil {
		log.Error(err)
	}
	return t
}

func GetBeginningToday() (timeYesterday time.Time) {
	timeStrBeginningYesterday := GetTimeNow().Format("2006-01-02 00:00:00")
	t, err := time.Parse("2006-01-02 00:00:00", timeStrBeginningYesterday)
	if err != nil {
		log.Error(err)
	}
	return t
}


func GetTomorrow() (timeTomorrow time.Time) {
	timeStrTomorrow := GetTimeNow().Add(24*time.Hour).Format("2006-01-02 15:04:05")
	t, err := time.Parse("2006-01-02 15:04:05", timeStrTomorrow)
	if err != nil {
		log.Error(err)
	}
	return t
}
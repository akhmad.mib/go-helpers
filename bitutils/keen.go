package bitutils

import (
	"gopkg.in/inconshreveable/go-keen.v0"
	"time"
)

const keenFlushInterval = 10 * time.Second

func GetKeenClient()(*keen.Client){
	return keenClient
}

func GetKeenBatchClient()(*keen.BatchClient){
	return keenBatchClient
}

func AddKeenEvent(collectionName string,data interface{}){
	keenClient.AddEvent(collectionName, data)
}

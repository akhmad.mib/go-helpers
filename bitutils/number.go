package bitutils

import (
	"math/rand"
	"time"
	"math/big"
	"fmt"
	"strconv"
	"math"
	"regexp"
)


func RandomInt(min int, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max-min) + min
}

func RandomIntFromLength(length int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(length)
}

func ParseFloat32(str string) float32 {
	x, _ := strconv.ParseFloat(str, 32)
	return float32(x)
}

func RoundFloat32(x1 float32, prec int) float32 {
	format := "%."+fmt.Sprint(prec)+"f"
	log.Info(format)
	z := fmt.Sprintf(format, x1)
	x, _ := strconv.ParseFloat(z, 64)
	if x == 0 {
		// Make sure zero is returned
		// without the negative bit set.
		return 0
	}
	// Fast path for positive precision on integers.
	if prec >= 0 && x == math.Trunc(x) {
		return float32(x)
	}
	pow := math.Pow10(prec)
	intermed := x * pow
	if math.IsInf(intermed, 0) {
		return float32(x)
	}
	if x < 0 {
		x = math.Ceil(intermed - 0.5)
	} else {
		x = math.Floor(intermed + 0.5)
	}

	if x == 0 {
		return 0
	}

	return float32(x / pow)
}

func PreciseFloat32(f float32, precision int) float32 {
	format := "%."+fmt.Sprint(precision)+"f"
	log.Info(format)
	z := fmt.Sprintf(format, f)
	value, err := strconv.ParseFloat(z, 32)
	if err != nil {
		// do something sensible
	}
	log.Info(z)
	f2 := float32(value)
	log.Info(f2)
	return f2
}

func RandomFloat64(min int, max int) float64 {
	return float64(RandomInt(min,max))
}

func NewFloat(money interface{}) (*big.Float){
	var moneyFloat float64
	moneyStr := fmt.Sprintf("%v",money)
	log.Info("displayMoney")
	log.Info(moneyStr)
	moneyFloat,_ = strconv.ParseFloat(moneyStr, 64);
	return  big.NewFloat(moneyFloat)//new(big.Float).SetPrec(2).SetString(moneyStr)
}

func GetOnlyNumber(str string) string {
	reg, err := regexp.Compile("[^0-9]+")
	if err != nil {
		log.Error(err)
		return ""
	}
	processedString := reg.ReplaceAllString(str, "")
	return processedString
}
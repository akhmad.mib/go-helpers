package bitutils

import (
	"bytes"
	"fmt"
	"html"
	"html/template"
	"net/url"
	"strconv"
	"math"
)

type Pagination struct {
	perPage     int
	totalAmount int
	currentPage int
	totalPage   int
	baseUrl     string

	// render parts
	firstPart   []string
	middlePart  []string
	lastPart    []string
	isJavascript bool
}

// constructor
func NewPagination(totalAmount, perPage, currentPage int, isJs bool, baseActionString string) *Pagination {
	if currentPage == 0 {
		currentPage = 1
	}

	n := int(math.Ceil(float64(totalAmount) / float64(perPage)))
	if currentPage > n {
		currentPage = n
	}

	return &Pagination{
		perPage:     perPage,
		totalAmount: totalAmount,
		currentPage: currentPage,
		totalPage:int(math.Ceil(float64(totalAmount) / float64(perPage))),
		baseUrl: baseActionString,
		isJavascript:isJs,
	}
}

// 总共几页
func (p *Pagination) TotalPages() int {
	return p.totalPage
}

// 是否要显示分页
func (p *Pagination) HasPages() bool {
	return p.TotalPages() > 1
}



const tmpl string = `
{{ if .HasPages }}
<ul class="pagination" style="margin:0; padding:0">
  {{ .GetPreviousButton "<" }}
  {{ range .FirstPart }}
    {{ . }}
  {{ end }}
  {{ if len .MiddlePart }}
    {{ .GetDots }}
	{{ range .MiddlePart }}
	  {{ . }}
	{{ end }}
  {{ end }}
  {{ if len .LastPart }}
	{{ .GetDots }}
	{{ range .LastPart }}
	  {{ . }}
	{{ end }}
  {{ end }}
  {{ .GetNextButton ">" }}
</ul>
{{end}}
`

const onEachSide int = 3

func (p *Pagination)FirstPart() []string {
	return p.firstPart
}
func (p *Pagination)MiddlePart() []string {
	return p.middlePart
}
func (p *Pagination)LastPart() []string {
	return p.lastPart
}
func (p *Pagination) generate() {
	if !p.HasPages() {
		return
	}
	if p.TotalPages() < (onEachSide * 2 + 6) {    // 11页以内
		p.firstPart = p.getUrlRange(1, p.TotalPages())
	}else {
		window := onEachSide * 2
		lastPage := p.TotalPages()
		if p.currentPage < window {    // 靠近开头
			p.firstPart = p.getUrlRange(1, window + 2)
			p.lastPart = p.getUrlRange(lastPage - 1, lastPage)
		}else if p.currentPage > (lastPage - window) {    // 靠近结尾
			p.firstPart = p.getUrlRange(1, 2)
			p.lastPart = p.getUrlRange(lastPage - (window + 2), lastPage)
		}else {    // 在中间
			p.firstPart = p.getUrlRange(1, 2)
			p.middlePart = p.getUrlRange(p.currentPage - onEachSide, p.currentPage + onEachSide)
			p.lastPart = p.getUrlRange(lastPage - 1, lastPage)
		}
	}
}

func (p *Pagination)getUrlRange(start, end int) []string {
	var ret []string
	for i := start; i <= end; i++ {
		ret = append(ret, p.getUrl(i, strconv.Itoa(i)))
	}
	return ret
}

func (p *Pagination)getUrl(page int, text string) string {
	strPage := strconv.Itoa(page)
	if p.currentPage == page {
		return p.GetActivePageWrapper(strPage)
	} else {
		if p.isJavascript {
			actionJs := fmt.Sprintf(p.baseUrl,page)
			return p.GetAvailablePageJavascriptWrapper(actionJs, text)
		}else {
			baseUrl, _ := url.Parse(p.baseUrl)
			params := baseUrl.Query()
			delete(params, "page")
			strParam := ""
			for k, v := range params {
				strParam = strParam + "&" + k + "=" + v[0]    // TODO
			}
			href := baseUrl.Host + "?page=" + strPage + strParam
			return p.GetAvailablePageWrapper(href, text)
		}
	}
}

func (p *Pagination) GetActivePageWrapper(text string) string {
	return "<li class=\"paginate_button active\"><span>" + text + "</span></li>"
}
func (p *Pagination) GetDisabledPageWrapper(text string) string {
	return "<li class=\"paginate_button disabled\"><span>" + text + "</span></li>"
}
func (p *Pagination) GetAvailablePageWrapper(href, page string) string {
	return "<li class=\"paginate_button \"><a href=\"" + href + "\">" + page + "</a></li>"
}
func (p *Pagination) GetAvailablePageJavascriptWrapper(actionJs, page string) string {
	return "<li class=\"paginate_button \"><a href=\"javascript:void(0);\"   onclick=\"" + actionJs + "\">" + page + "</a></li>"
}
func (p *Pagination) GetDots() string {
	return "<li class=\"paginate_button disabled\"><span>...</span></li>"
}
func (p *Pagination)GetPreviousButton(text string) string { // "&laquo;"
    text = "Previous"
	if p.currentPage <= 1 {
		return p.GetDisabledPageWrapper(text)
	}

	return p.getUrl(p.currentPage - 1, text)//"<")
}
func (p *Pagination)GetNextButton(text string) string {    // &raquo;
	text = "Next"
	if p.currentPage == p.TotalPages() {
		return p.GetDisabledPageWrapper(text)
	}
	return p.getUrl(p.currentPage + 1, text) //">")
}

// 生成html
func (p *Pagination) Render() template.HTML {
	p.generate()

	var out bytes.Buffer
	t := template.Must(template.New("pagination").Parse(tmpl))
	err := t.Execute(&out, p)
	if err != nil {
		return template.HTML(fmt.Sprintf("Error executing pagination template: %s", err))
	}
	return template.HTML(html.UnescapeString(out.String()))
}

package bitutils

import (
	"bytes"
	"gitlab.com/akhmad.mib/go-helpers/bitconfigs"
	"html/template"
	"os"
	"path/filepath"
)

func InitEmailUtil(){
	mailgunConfig := bitconfigs.MailgunConfig{}
	mailgunConfig.MailgunDomain = mailgunDomain
	mailgunConfig.MailgunApiKey = mailgunApiKey
	mailgunConfig.MailgunPublicApiKey = mailgunPublicApiKey
	mailgunConfig.MailgunEmailFrom = mailgunEmailFrom
	InitEmailMailGunConfig(&mailgunConfig)
}


func ParseEmailTemplate(templateFileName string, data interface{}) (body string,err error) {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	pathFile := dir+"/views/email_templates/"+templateFileName
	t, err := template.New(templateFileName).Funcs(GetAllFuncMap()).ParseFiles(pathFile)
	if err != nil {
		return body,err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return body,err
	}
	body = buf.String()
	return
}
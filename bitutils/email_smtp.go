package bitutils

import (
	"fmt"
	"gitlab.com/akhmad.mib/go-helpers/bitobjects"
	"net/mail"
	"net/smtp"
	"strings"
	//"encoding/base64"
)

func encodeRFC2047(String string) string {
	// use mail's rfc2047 to encode any string
	addr := mail.Address{String, ""}
	return strings.Trim(addr.String(), " <>")
}

func EmailSmtpSendHTML(req bitobjects.Email) (err error) {
	//source : https://gist.github.com/andelf/5004821
	auth := smtp.PlainAuth("", smtpUsername, smtpPassword, smtpHost)
	//from := mail.Address{"监控中心", "fledna@163.com"}
	//to := mail.Address{"收件人", "name@139.com"}
	//title := req.Subject

	header := make(map[string]string)
	header["From"] = req.From
	//header["To"] = req.To[0]
	header["Subject"] = req.Subject //encodeRFC2047(req.Subject)
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = "text/html; charset=\"utf-8\""
	//header["Content-Transfer-Encoding"] = "base64"
	message := ""
	for k, v := range header {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	msg := []byte(message + "\n" + req.HTML)
	addr := fmt.Sprintf("%s:%d", smtpHost, smtpPort)

	if err := smtp.SendMail(addr, auth, emailSender, req.To, msg); err != nil {
		return err
	}
	return nil
}

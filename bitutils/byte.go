package bitutils

import (
	"bytes"
	"encoding/gob"
	"strconv"
	"reflect"
)

func GetBytes(data interface{}) ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(data)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func GetInterface(bts []byte, data interface{}) error {
	buf := bytes.NewBuffer(bts)
	dec := gob.NewDecoder(buf)
	err := dec.Decode(data)
	if err != nil {
		return err
	}
	return nil
}

func IntStringToHexString(s string) (string){
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		panic(err)
	}
	return IntToHexString(i)
}

func IntToHex(n int64) []byte {
	return []byte(strconv.FormatInt(n, 16))
}

func IntToHexString(n int64) string {
	return string(IntToHex(n))
}

func IsEmptyOrNil(x interface{}) bool {
	return x==nil || reflect.DeepEqual(x, reflect.Zero(reflect.TypeOf(x)).Interface())
}
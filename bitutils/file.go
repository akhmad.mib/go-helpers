package bitutils

import (
	"os"
	"net/http"
	"path/filepath"
)

func GetFileContentType(pathFile string) (string, error) {
	// Open File
	out, err := os.Open(pathFile)
	if err != nil {
		panic(err)
	}
	defer out.Close()
	// Only the first 512 bytes are used to sniff the content type.
	buffer := make([]byte, 512)

	_, err = out.Read(buffer)
	if err != nil {
		return "", err
	}

	// Use the net/http package's handy DectectContentType function. Always returns a valid
	// content-type by returning "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer)

	return contentType, nil
}

func GetFileNameFromPath(filename string) (string){
	var extension = filepath.Ext(filename)
	var name = filename[0:len(filename)-len(extension)]
	return name
}
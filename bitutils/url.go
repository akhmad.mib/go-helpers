package bitutils

import (
	"github.com/gosimple/slug"
)

func ConvertToPermalink(str string)(string){
	//salt := time.Now().Format("15-04-05")
	salt := RandomStringMinMax(5,8)
	return slug.Make(str)+"-"+salt
}
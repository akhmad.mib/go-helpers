package bitutils

import (
	hex2 "encoding/hex"
	"math/rand"
	"regexp"
	"strings"
	"time"
)

func RandomStringMinMax(min int, max int) string {
	randomInt := RandomInt(min,max)
	return RandomStringFixedLength(randomInt)
}

func RandomString() string {
	min := 6
	max := 11
	return RandomStringMinMax(min,max)
}

func RandomStringFixedLength(n int) string {
	return RandStringBytesMaskImprSrc(n)
}

//https://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-golang
const letterNumberRunes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0987654321"

//method 1 random bytes
func RandomStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterNumberRunes[rand.Intn(len(letterNumberRunes))]
	}
	return string(b)
}

//method 2 using remainder
func RandStringBytesRmndr(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterNumberRunes[rand.Int63() % int64(len(letterNumberRunes))]
	}
	return string(b)
}

const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)


//method 3 using masking
func RandStringBytesMask(n int) string {
	b := make([]byte, n)
	for i := 0; i < n; {
		if idx := int(rand.Int63() & letterIdxMask); idx < len(letterNumberRunes) {
			b[i] = letterNumberRunes[idx]
			i++
		}
	}
	return string(b)
}

//method 4 using masking improvement
func RandStringBytesMaskImpr(n int) string {
	b := make([]byte, n)
	// A rand.Int63() generates 63 random bits, enough for letterIdxMax letters!
	for i, cache, remain := n-1, rand.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = rand.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterNumberRunes) {
			b[i] = letterNumberRunes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

var srcTimeRand = rand.NewSource(time.Now().UnixNano())

//method 5 using masking improvement source
func RandStringBytesMaskImprSrc(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, srcTimeRand.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = srcTimeRand.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterNumberRunes) {
			b[i] = letterNumberRunes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

func ToValidUTF8(s string) string {
	s = CleanAllNonAsciiCharacter(s)
	return strings.Map(func(r rune) rune { return r }, s)
}

func CleanAllNonAsciiCharacter(s string) string {
	re := regexp.MustCompile("[[:^ascii:]]")
	t := re.ReplaceAllLiteralString(s, "")
	return t
}

func GetOnlyAlphanumeric(str string) string {
	reg, err := regexp.Compile("[^a-zA-Z0-9]+")
	if err != nil {
		log.Panic(err)
	}
	processedString := reg.ReplaceAllString(str, "")
	return processedString
}

func IsArrayStringContains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func HexToString(hex string)(string){
	ba,err := hex2.DecodeString(hex)
	if err != nil {
		log.Info(err)
		return ""
	}
	return string(ba)
}
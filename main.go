package main

import (
	"gitlab.com/akhmad.mib/go-helpers/bitlogging"
	"gitlab.com/akhmad.mib/go-helpers/bitutils"
)

var(
	log = bitlogging.MustGetLogger()
)

func main() {
	var err error
	log.Info(err)
	bitutils.InitAllByConfigs()
}
package bitrequests

import (
	"time"
	"net/http"
)

type RequestList struct {
	Fields []string
	Sortby []string
	Order []string
	Query map[string]string
	Limit int64
	Offset int64
	Search string
	OrderBy string
	StartDate time.Time
	EndDate	time.Time
}

type DefaultRequest struct {
	Url    string
	Params map[string]string
	Body   interface{}
	Method string
	Header http.Header
}

type AuthorizationToken struct {
	Authorization
	AuthorizationHeader
}

type Authorization struct {
	DefaultRequest
}

type AuthorizationHeader struct {
	JwtToken string `json:"JwtToken"`
}
package bitrequests

type CommonRequest struct {
	IsAdmin		bool
	IsShowAll	bool
	ScopeShow	string // "" all data, "1" scoped, "2" unscoped
}

type IdRequest struct {
	Id 			string
	UserId  	string
	CommonRequest
}

type NameRequest struct {
	Name		string
	UserId  	string
	CommonRequest
}
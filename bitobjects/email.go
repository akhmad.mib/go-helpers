package bitobjects

import (
	"net/smtp"
	"net/textproto"
)

type Email struct {
	EmailBase
}

type EmailBase struct {
	Auth        smtp.Auth
	Identity    string `json:"identity"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	Host        string `json:"host"`
	Port        int    `json:"port"`
	From        string `json:"from"`
	To          []string
	Bcc         []string
	Cc          []string
	Subject     string
	Text        string // Plaintext message (optional)
	HTML        string // Html message (optional)
	Headers     textproto.MIMEHeader
	Attachments []*Attachment
	ReadReceipt []string
}

type EmailLandingPageForm struct {

}

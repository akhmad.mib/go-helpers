package bitobjects

type MinioFile struct {
	Name        string
	BucketName  string
	RegionName  string
	PathName    string
	ContentType string
	SizeFile    int64
}

type MinioFileUpload struct {
	Name        string
	BucketName  string
	RegionName  string
	PathName    string
	SourcePath  string
	ContentType string
}

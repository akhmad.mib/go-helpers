package bitobjects

import "time"

const ATTACHMENT_PRODUCT_IMAGE = "PRODUCT_IMAGE"
const ATTACHMENT_USER = "USER_FILE"
const ATTACHMENT_SUPPORT = "SUPPORT_FILE"
const ATTACHMENT_BOTSOL = "BOTSOL"

const BUCKET_ATTACHMENT_PRODUCT_IMAGE = "product-images"
const BUCKET_ATTACHMENT_USER = "files"
const BUCKET_ATTACHMENT_SUPPORT = "support"
const BUCKET_ATTACHMENT_BOTSOL = "botsol"


type Attachment struct {
	Id             string `gorm:"type:varchar(50);primary_key"`
	Name           string `gorm:"type:varchar(400)"`
	Code           string `gorm:"type:varchar(50)";null`
	BucketName     string `gorm:"type:varchar(50)"`
	RegionName     string `gorm:"type:varchar(50)"`
	AttachmentType string `gorm:"type:varchar(50)"`      // product_image, support, user_attachment
	FileType       string `gorm:"type:varchar(50);null"` // image, pdf, zip, doc, xls?
	ContentType    string `gorm:"type:varchar(50)"`
	PathFile       string `gorm:"type:varchar(400)"`
	ExternalUrl    string `gorm:"type:varchar(400)"` //misal ambil dari url luar, bukan dari minio
	IsTemporary	   bool
	SizeFile       int64
	CreatedAt      time.Time
}
package bitobjects

type ImgProxyFile struct{
	Resize string
	Width int
	Height int
	Gravity string
	Enlarge int
	Extension string
	Url string
}


package bitobjects


type KeyValueOption struct {
	Key 	interface{}
	Value 	interface{}
}

func(kv KeyValueOption) KeyInt()(int){
	return kv.Key.(int)
}

func(kv KeyValueOption) KeyString()(string){
	return kv.Key.(string)
}

func(kv KeyValueOption) ValueInt()(int){
	return kv.Value.(int)
}

func(kv KeyValueOption) ValueString()(string){
	return kv.Value.(string)
}